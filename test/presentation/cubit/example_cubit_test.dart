import 'package:error_handling/models/example_exception.dart';
import 'package:error_handling/presentation/cubit/example_cubit.dart';

import 'package:error_handling/repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'example_cubit_test.mocks.dart';

@GenerateMocks([Repository])
void main() {
  final Repository repository = MockRepository();
  final cubit = ExampleCubit(repository);
  test(
      'Should there be no internet connection, NoInternet State should be emitted',
      () {
    when(repository.call())
        .thenThrow(ExampleException(ExampleError.noInternetConnection));
    expectLater(
      cubit.stream,
      emitsInOrder([
        const ExampleState.loading(),
        const ExampleState.noInternet(),
      ]),
    );
    cubit.exampleFunction();
  });
  test('Should there be a random error, Error State is emitted', () {
    when(repository.call()).thenThrow(ExampleException(ExampleError.unknown));
    expectLater(
      cubit.stream,
      emitsInOrder([
        const ExampleState.loading(),
        const ExampleState.error('unknown error'),
      ]),
    );
    cubit.exampleFunction();
  });
  test('If there are no errors, success state should be emitted', () {
    when(repository.call())
        .thenAnswer((realInvocation) => Future.value('test'));
    expectLater(
      cubit.stream,
      emitsInOrder([
        const ExampleState.loading(),
        const ExampleState.success(),
      ]),
    );
    cubit.exampleFunction();
  });
}
