enum ExampleError {
  noInternetConnection,

  unknown,
}

class ExampleException implements Exception {
  final ExampleError error;

  ExampleException(this.error);
}
