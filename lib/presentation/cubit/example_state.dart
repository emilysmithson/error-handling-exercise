part of 'example_cubit.dart';

@freezed
class ExampleState with _$ExampleState {
  const factory ExampleState.initial() = _Initial;
  const factory ExampleState.success() = _Success;
  const factory ExampleState.noInternet() = _NoInternet;
  const factory ExampleState.loading() = _Loading;
  const factory ExampleState.error(String message) = _Error;
}
