import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../models/example_exception.dart';
import '../../repository.dart';

part 'example_state.dart';
part 'example_cubit.freezed.dart';

class ExampleCubit extends Cubit<ExampleState> {
  ExampleCubit(this.repository) : super(const ExampleState.initial());
  final Repository repository;

  exampleFunction() {
    emit(const ExampleState.loading());
    try {
      repository.call();
    } on ExampleException catch (e) {
      switch (e.error) {
        case ExampleError.noInternetConnection:
          emit(const ExampleState.noInternet());
          break;

        case ExampleError.unknown:
          emit(const ExampleState.error('unknown error'));
          break;
      }
      return;
    }
    emit(const ExampleState.success());
  }
}
